/*
===============================================================================
= Ground constructor ==========================================================
===============================================================================
*/
define('ground/ground', function()
{    		   
    return function()
    {
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/ground/ground.json', function( geometry )
        {
			var material = new THREE.MeshPhongMaterial(
            {
                shading: THREE.FlatShading// takie ustawienie jest konieczne dla pozbycia sie smug                                
                ,color: 0x003f73//
                ,reflectivity: 0.3
                
            });

			ground = new THREE.Mesh( geometry, material );
			ground.scale.set( 40, 1, 40 );
			ground.position.set(0, -40,0);
			ground.rotation.set(0, 0, 0);
			ground.receiveShadow = true;            

            //sceneWebGl.add( ground );
		});
        

    }
              
});