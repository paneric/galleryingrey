/*
===============================================================================
= Ground constructor ==========================================================
===============================================================================
*/
define('wall/wall', function()
{    
    return function()
    {		
        var textureLoader = new THREE.TextureLoader();
    	bricsTexture = textureLoader.load(base_url + 'threejs/module/hall/wall/brics.jpg' );
    	
    //---------------------------------------------------------------------
    /** r2wS */ // duza ceglana
    //---------------------------------------------------------------------       
        
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/wall/r2wS.json', function( geometry ){
			
	       bricsTexture.wrapS = bricsTexture.wrapT = THREE.RepeatWrapping; 
           //r2wSBump.wrapS = r2wSBump.wrapT = THREE.RepeatWrapping;
	       
           bricsTexture.repeat.set( 8, 8 ); //15.6 x 7.6           
           //r2wSBump.repeat.set( 8.21, 4 ); //15.6 x 7.6             
            
            var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,map : bricsTexture
                //,bump : r2wSBump
                ,shininess  :  25
                ,bumpScale  :  1
                //,emissive   :  new THREE.Color("rgb(7,3,5)")
                //,specular   :  new THREE.Color("rgb(255,113,0)")
                //,side : THREE.DoubleSide,
                //,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.99
                //,shininess: 10                
            });
			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.receiveShadow = true;
			wall.castShadow = true;			
            
			sceneWebGl.add( wall );
		});
        
    //---------------------------------------------------------------------
    /** r2wW1 */
    //---------------------------------------------------------------------          
        
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/wall/r2wW1.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,reflectivity: 0.3 
                //,shininess: 10                
            });
			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.receiveShadow = true;
			wall.castShadow = true;	
            
			sceneWebGl.add( wall );
		});
        
    //---------------------------------------------------------------------
    /** r2wW2 */
    //--------------------------------------------------------------------- 
     
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/wall/r2wW2.json', function( geometry ){
			
	       bricsTexture.wrapS = bricsTexture.wrapT = THREE.RepeatWrapping; 
	       bricsTexture.repeat.set( 8, 8 );  

            var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,map: bricsTexture
                //,side : THREE.DoubleSide,
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,reflectivity: 0.99
                //,shininess: 10                
            });
			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.receiveShadow = true;
			wall.castShadow = true;	
            
			sceneWebGl.add( wall );
		});        
        
    //---------------------------------------------------------------------
    /** r2wW3 */
    //---------------------------------------------------------------------
            
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/wall/r2wW3.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,reflectivity: 0.3 
                //,shininess: 10                 
            });
			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.receiveShadow = true;
			wall.castShadow = true;	
            
			sceneWebGl.add( wall );
		}); 
        
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/wall/r2fW3.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0x003f73 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3
                //,shininess: 10                 
            });
			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.receiveShadow = true;
			wall.castShadow = true;	
            
			sceneWebGl.add( wall );
		});        
        
    //---------------------------------------------------------------------
    /** r2wE1 */
    //---------------------------------------------------------------------        

        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/wall/r2wE1.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3 
                //,shininess: 10                
            });
			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.receiveShadow = true;
			wall.castShadow = true;	
            
			sceneWebGl.add( wall );
		});

    //---------------------------------------------------------------------
    /** r2wE2 */
    //---------------------------------------------------------------------

        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/wall/r2wE2.json', function( geometry ){

            bricsTexture.wrapS = bricsTexture.wrapT = THREE.RepeatWrapping; 
            bricsTexture.repeat.set( 8, 8 );    
            
            var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,map: bricsTexture
                //,side : THREE.DoubleSide,
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3
                //,shininess: 10                 
            });
			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.receiveShadow = true;
			wall.castShadow = true;	
            
			sceneWebGl.add( wall );
		});
         
    //---------------------------------------------------------------------
    /** r2wE3 */
    //---------------------------------------------------------------------
        
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/wall/r2wE3.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3 
                //,shininess: 10                                
            });
			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.receiveShadow = true;
			wall.castShadow = true;	
            
			sceneWebGl.add( wall );
		});

        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/wall/r2fE3.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0x003f73  
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3
                //,shininess: 10                  
            });
			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.receiveShadow = true;
			wall.castShadow = true;	
            
			sceneWebGl.add( wall );
		});

    //---------------------------------------------------------------------
    /** r2wN1 */
    //---------------------------------------------------------------------

        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/wall/r2wN1.json', function( geometry ){            
            
        bricsTexture.wrapS = bricsTexture.wrapT = THREE.RepeatWrapping; 
        bricsTexture.repeat.set( 8, 8 );      
            
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0xffffff
                ,map : bricsTexture  
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3
                //,shininess: 10                                 
            });
			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.castShadow = true;
			wall.receiveShadow = true;
			wall.castShadow = true;	
            
			sceneWebGl.add( wall );                        
		});

    //---------------------------------------------------------------------
    /** r2wN2 */
    //---------------------------------------------------------------------
        
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/wall/r2wN2.json', function( geometry ){            
            
        bricsTexture.wrapS = bricsTexture.wrapT = THREE.RepeatWrapping; 
        bricsTexture.repeat.set( 8, 8 );            
            
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0xffffff
                ,map : bricsTexture  
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3
                //,shininess: 10                                 
            });
			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.castShadow = true;
            wall.receiveShadow = true;            
            
			sceneWebGl.add( wall );                        
		});
        
    //---------------------------------------------------------------------
    /** door */
    //---------------------------------------------------------------------

        var loader = new THREE.ColladaLoader();
        
        loader.load(base_url + 'threejs/module/hall/wall/r2wNd.dae', function colladaReady( collada ) 
        {   
			//collada.scene.castShadow = true;            
            
            var glass1 = collada.scene.children[0].children[0];
            glass1.scale.set( 100, 100, 100 );
			glass1.position.set(0, 0, -700);
			glass1.rotation.set(-Math.PI/2, 0, 0);          
            glass1.material.color.set(0xffffff);
            glass1.material.opacity = 0.65;
            glass1.material.transparent = true; 
            
            var glass2 = collada.scene.children[4].children[0];
            glass2.scale.set( 100, 100, 100 );
			glass2.position.set(0, 0, -700);
			glass2.rotation.set(-Math.PI/2, 0, 0);          
            glass2.material.color.set(0xffffff); 
            glass2.material.opacity = 0.65;
            glass2.material.transparent = true;           

            var frame1 = collada.scene.children[2].children[0];
            frame1.scale.set( 100, 100, 100 );
			frame1.position.set(0, 0, -700);
			frame1.rotation.set(-Math.PI/2, 0, 0);            
            frame1.material.color.set(0xffffff);            
                    
            var door1 = collada.scene.children[1].children[0];
            door1.scale.set( 100, 100, 100 );
			door1.position.set(0, 0, -700);
			door1.rotation.set(-Math.PI/2, 0, 0);               
            door1.material.color.set(0xffffff); 
            
            var door2 = collada.scene.children[3].children[0];
            door2.scale.set( 100, 100, 100 );
			door2.position.set(0, 0, -700);
			door2.rotation.set(-Math.PI/2, 0, 0);               
            door2.material.color.set(0xffffff);            
            
            sceneWebGl.add( glass1 );
            sceneWebGl.add( glass2 );             
            sceneWebGl.add( frame1 ); 
            sceneWebGl.add( door1 );
            sceneWebGl.add( door2 );                    
             
        });    

        loader.load(base_url + 'threejs/module/hall/wall/r2wNd.dae', function colladaReady( collada ) 
        {   
			//collada.scene.castShadow = true;            

            var glass1 = collada.scene.children[0].children[0];
            glass1.scale.set( 100, 100, 100 );
			glass1.position.set(0, 400, -1100);
			glass1.rotation.set(-Math.PI/2, 0, 0);          
            glass1.material.color.set(0xffffff);
            glass1.material.opacity = 0.65;
            glass1.material.transparent = true; 
            
            var glass2 = collada.scene.children[4].children[0];
            glass2.scale.set( 100, 100, 100 );
			glass2.position.set(0, 400, -1100);
			glass2.rotation.set(-Math.PI/2, 0, 0);          
            glass2.material.color.set(0xffffff); 
            glass2.material.opacity = 0.65;
            glass2.material.transparent = true;            

            var frame1 = collada.scene.children[2].children[0];
            frame1.scale.set( 100, 100, 100 );
			frame1.position.set(0, 400, -1100);
			frame1.rotation.set(-Math.PI/2, 0, 0);            
            frame1.material.color.set(0xffffff);            
                    
            var door1 = collada.scene.children[1].children[0];
            door1.scale.set( 100, 100, 100 );
			door1.position.set(0, 400, -1100);
			door1.rotation.set(-Math.PI/2, 0, 0);               
            door1.material.color.set(0xffffff); 
            
            var door2 = collada.scene.children[3].children[0];
            door2.scale.set( 100, 100, 100 );
			door2.position.set(0, 400, -1100);
			door2.rotation.set(-Math.PI/2, 0, 0);               
            door2.material.color.set(0xffffff);            

            sceneWebGl.add( glass1 );
            sceneWebGl.add( glass2 );             
            sceneWebGl.add( frame1 ); 
            sceneWebGl.add( door1 );
            sceneWebGl.add( door2 );                     
        });  
                 
    //---------------------------------------------------------------------
    /** r2f2 */
    //---------------------------------------------------------------------
        
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/wall/r2f2.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0x003f73 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3
                //,shininess: 10                                 
            });
			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.receiveShadow = true;
			wall.castShadow = true;	
            
			//sceneWebGl.add( wall );
		});         
        
        /*        
        var loader = new THREE.ColladaLoader();
        loader.options.convertUpAxis = true;

        loader.load( 'threejs/module/wall/r2wN1.dae', function colladaReady( collada ) 
        {   
            wall = collada.scene.children[0].children[0];
            wall.material.map.repeat.set(10,2)
            wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.castShadow = true;
            
			sceneWebGl.add( wall ); 

        });
        
        */
    }        
});