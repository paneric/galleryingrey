/*
===============================================================================
= Ground constructor ==========================================================
===============================================================================
*/
define('module/hall/grid/grid', function()
{        
    return function()
    {
        var axes = new THREE.AxisHelper(200);
		axes.position.set(0, 0, 0);
		sceneWebGl.add(axes);

		var gridXZ = new THREE.GridHelper(150, 10);
		gridXZ.setColors( new THREE.Color(0xFFC0CB), new THREE.Color(0x8f8f8f) );
		gridXZ.position.set(0,0,0 );
		sceneWebGl.add(gridXZ);                
    };    
});