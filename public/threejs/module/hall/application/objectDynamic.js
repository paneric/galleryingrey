/*
===============================================================================
= ObjectWebGl constructor =====================================================
===============================================================================
*/
define('application/objectDynamic',[

	'horizon/horizon'
	,'mainsign/mainsign'
    ,'hall/ui/icon'                    
	], 
	function(
        
        horizon
        ,mainsign
        ,icon
    )
	{	
        return function()
        {
            sceneWebGl.add(horizon.obj);
            sceneWebGl.add(mainsign.obj);
            sceneWebGl.add(arrowIcon.obj);        
                
            objectDynamic = 
            {
                horizon : horizon
                ,mainsign : mainsign               
            }                                     
        }           
	}
);