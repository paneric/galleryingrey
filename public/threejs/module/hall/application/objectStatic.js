/*
===============================================================================
= SpaceWebGl constructor ======================================================
===============================================================================
*/
define('application/objectStatic',[

    'floor/floor'
	,'ceiling/ceiling'
	,'wall/wall'
    ,'stairs/stairs'
    ,'truss/truss'
    ,'paint/paint'
    ,'gui/panel/hallpanel'  
    //,'grid/grid'                
	
    ], function(
    
        floor
        ,ceiling
        ,wall
        ,stairs
        ,truss
        ,paint
        ,hallpanel
        //,grid
    )
    {
        return function()
        {   
            floor();
            ceiling();
            wall();
            stairs();
            truss();
            paint();           
            hallpanel();
            //grid();            
        }
    }
);