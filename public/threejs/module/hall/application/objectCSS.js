/*
===============================================================================
= SpaceWebGl constructor ======================================================
===============================================================================
*/
define('module/hall/application/objectCSS',[

    'module/hall/hybrid/iconCSS'    
    ,'module/hall/hybrid/icon'              
	], 
	function(
	
        iconCSS
        ,icon     		
	)
	{
        return function()
        {
            icon(iconCSS());                
        }
    }
);