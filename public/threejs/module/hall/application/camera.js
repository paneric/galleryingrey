define('application/camera', function()
{
    return function()
    {
        // cameraWebGl : --------------------------------------------------------------
        //-----------------------------------------------------------------------------
        cameraWebGl = new THREE.PerspectiveCamera( 80, window.innerWidth / window.innerHeight, 0.1, 10000 );//80
        cameraWebGl.position.set(0,180,500);
        sceneWebGl.add(cameraWebGl);        

        // groundCubeCamera : ---------------------------------------------------------
        //-----------------------------------------------------------------------------    
        r2f1CubeCamera = new THREE.CubeCamera(0.1, 3000,1024); // parameters: near, far, resolution = 2048
        //r2f1CubeCamera.renderTarget.minFilter = THREE.LinearFilter; // mipmap filter
        r2f1CubeCamera.renderTarget.texture.minFilter = THREE.LinearFilter; // mipmap filter        
        r2f1CubeCamera.position.set(0, 0, 500);
        r2f1CubeCamera.rotation.set(0,0 ,0 );
        r2f1CubeCamera.scale.set(1,1,1);             
        r2f1CubeCamera.lookAt(new THREE.Vector3(0,0,500));
        
        sceneWebGl.add(r2f1CubeCamera);
        
        // glassCubeCamera : ---------------------------------------------------------
        //-----------------------------------------------------------------------------    
        glassCubeCamera = new THREE.CubeCamera(0.1, 3000,1024); // parameters: near, far, resolution = 2048
        //r2f1CubeCamera.renderTarget.minFilter = THREE.LinearFilter; // mipmap filter
        glassCubeCamera.renderTarget.texture.minFilter = THREE.LinearFilter; // mipmap filter        
        //glassCubeCamera.position.set(0, 0, 0);
        glassCubeCamera.rotation.set(0,0 ,0 );
        //glassCubeCamera.rotation.x = Math.PI/2;
        //glassCubeCamera.lookAt(new THREE.Vector3(0,500,0));        
console.log(glassCubeCamera);        
        glassCubeCamera.scale.set(1,1,1);             
        glassCubeCamera.lookAt(new THREE.Vector3(500,0,0));
        
        sceneWebGl.add(glassCubeCamera);        
        
        
    }           
});