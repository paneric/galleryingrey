define('application/animate',[
    
        //'application/objectAnimate'
    
    ], function(
    
        //objectAnimate
    )
    {                   
		render = function render()
        {                     
            rendererWebGl.render(sceneWebGl,cameraWebGl);
            rendererCSS.render(sceneCSS,cameraWebGl ); // CSS !!!                           
        };        

        return function animate()
        {   
            circle.visible = false;
            //glassPanel.visible = false;
            
            r2f1CubeCamera.position.z = cameraWebGl.position.z;
            r2f1CubeCamera.position.x = cameraWebGl.position.x;                                                        
            r2f1CubeCamera.updateCubeMap(rendererWebGl, sceneWebGl);
            
            /*
            glassCubeCamera.position.copy(glassPanel.position);
            glassCubeCamera.rotation.copy(glassPanel.rotation);             
            glassCubeCamera.updateCubeMap(rendererWebGl, sceneWebGl);            
            */
            
            circle.visible = true;
            //glassPanel.visible = true;            
            
            requestAnimationFrame(animate);
            //objectAnimate();
            
            controlsWebGl.update( 1 );            
            
            render();
        };
    }
);