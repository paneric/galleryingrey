/*
===============================================================================
= LADOWANIE RENDEROW ==========================================================
===============================================================================
*/
define('application/appendRenderer',function()
{
    return function()
    {       
        //Let�s add a code snippet inside function init () so that whenever the user resizes 
        //his browser window the projection matrix will be updated.
		window.addEventListener('resize', function()
		{
			rendererWebGl.setSize(window.innerWidth, window.innerHeight);
            		
			cameraWebGl.aspect = window.innerWidth / window.innerHeight;
			cameraWebGl.updateProjectionMatrix();
            
			rendererCSS.setSize(window.innerWidth, window.innerHeight); // CSS !!!            
		});
        
        document.body.appendChild( rendererWebGl.domElement );
        document.body.appendChild( rendererCSS.domElement ); // CSS !!!       
        
        rendererWebGl.domElement.addEventListener( 'click', onMouseClick );   
    }     	
});