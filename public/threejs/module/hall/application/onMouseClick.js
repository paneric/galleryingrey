/*
===============================================================================
= SceneWebGl constructor ======================================================
===============================================================================
*/
define('application/onMouseClick',function()
{	  
    onMouseClick = function(event_info)
    {
        //stop any other event listener from recieving this event
        event_info.preventDefault();  
    
        //this where begin to transform the mouse cordinates to three,js cordinates
        mouse.x = ( event_info.clientX / window.innerWidth ) * 2 - 1;
        mouse.y = - ( event_info.clientY / window.innerHeight ) * 2 + 1;
    
        //this vector caries the mouse click cordinates
        mouse_vector.set( mouse.x, mouse.y, mouse.z );
    
        //the final step of the transformation process, basically this method call
        //creates a point in 3d space where the mouse click occurd
        mouse_vector.unproject( cameraWebGl );
        
        var direction = mouse_vector.sub( cameraWebGl.position ).normalize();
    
        //ray = new THREE.Raycaster( camera.position, direction );
        ray.set( cameraWebGl.position, direction );
    
        //asking the raycaster if the mouse click touched the sphere object
        //intersects = ray.intersectObject( arrowIcon.obj, true );
    
        //the ray will return an array with length of 1 or greater if the mouse click
        //does touch the sphere object
        
        if( intersects.length )
        {
            //alert( "hit" );
            arrowIcon.obj.position.z = arrowIcon.obj.position.z +100;
            animate();
        }     
    }

});