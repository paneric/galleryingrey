/*
===============================================================================
= Camera constructor ==========================================================
===============================================================================
*/
define('application/light', function()
{
    return function()
    {   
        
        var spotLight = new THREE.SpotLight( 0xffffff,1 );
        spotLight.position.set( 390, 700, 230 );
        spotLight.target = picObj[1].picture;
        spotLight.angle = Math.PI/42;


        spotLight.castShadow = true;

        spotLight.shadow.mapSize.width = 1024;
        spotLight.shadow.mapSize.height = 1024;

        spotLight.shadow.camera.near = 500;
        spotLight.shadow.camera.far = 4000;
        spotLight.shadow.camera.fov = 30;

        sceneWebGl.add( spotLight );
        
        
        
        
        
        
        
        var light1o = new THREE.DirectionalLight(0xffffff,0.5);
		light1o.position.set(0,700,1100);//500
        //light1o.target.position.set(0 , 600, 0 );       
        
        light1o.castShadow = true;   
		//sceneWebGl.add(light1o);

        var sphereSize = 25; 
		var DirectionalLightHelper = new THREE.DirectionalLightHelper( light1o, sphereSize );
		//sceneWebGl.add( DirectionalLightHelper );
        
        
        var light2o = new THREE.DirectionalLight(0xffffff,0.5);
		light2o.position.set(0,700,-1100);//500        
        //light2o.target.position.set( 780, 160, 1100 );

        light2o.castShadow = true;
		//sceneWebGl.add(light2o);

		var sphereSize = 25; 
		var DirectionalLightHelper = new THREE.DirectionalLightHelper( light2o, sphereSize ); 
		//sceneWebGl.add( DirectionalLightHelper );
        
        
        var light3o = new THREE.DirectionalLight(0xffffff,0.5);
		light3o.position.set(-500,500,0);//500        
        light3o.target.position.set( -500, 0, 0 );

        light3o.castShadow = true;
		//sceneWebGl.add(light3o);

		var sphereSize = 25; 
		var DirectionalLightHelper = new THREE.DirectionalLightHelper( light3o, sphereSize ); 
		//sceneWebGl.add( DirectionalLightHelper );        
        
        
        var light4o = new THREE.DirectionalLight(0xffffff,0.5);
		light4o.position.set(500,500,0);//500        
        //light4o.target.position.set( toX, toY, toZ );

        light4o.castShadow = true;
		//sceneWebGl.add(light4o);

		var sphereSize = 25; 
		var DirectionalLightHelper = new THREE.DirectionalLightHelper( light4o, sphereSize ); 
		//sceneWebGl.add( DirectionalLightHelper );        

    }        
              
});