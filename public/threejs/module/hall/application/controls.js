/*
===============================================================================
= Controls constructor ==========================================================
===============================================================================
*/
define('application/controls', function()
{
    return function()
    {
        // ControlsWebGll : -----------------------------------------------------------
        //-----------------------------------------------------------------------------
        
        /*
        controlsWebGl = new THREE.FlyControls(cameraWebGl);	
        controlsWebGl.movementSpeed = 20;
        controlsWebGl.domElement = rendererWebGl.domElement;
        controlsWebGl.rollSpeed = 0.05;
        controlsWebGl.autoForward = false;//false
        controlsWebGl.dragToLook = true;//true
        */
        
        
        
        
       
        controlsWebGl = new THREE.OrbitControls(cameraWebGl, rendererWebGl.domElement);	

        controlsWebGl.maxPolarAngle = Math.PI/2;
        controlsWebGl.minPolarAngle = Math.PI/2;
        
        controlsWebGl.minDistance = 0;
        controlsWebGl.maxDistance = 1;
        
        //controlsWebGl.panUp(200);
        //controlsWebGl.pan(0,10);
        
        controlsWebGl.target.set(0,200,500);
        //controlsWebGl.target.set(-340,600,-950);
        //controlsWebGl.rotate.left(Math.PI);
        
        controlsWebGl.enableKeys = false;
        

        // ControlsCSS : --------------------------------------------------------------
        //-----------------------------------------------------------------------------         
    }   
});