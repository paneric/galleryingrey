/*
===============================================================================
= SceneWebGl constructor ======================================================
===============================================================================
*/
define('application/variables',function()
{	             
        // (1) Cameras : //----------------------------------------
        //---------------------------------------------------------     
        cameraWebGl = {};        
        r2f1CubeCamera = {};
        glassCubeCamera = {};
    
        // (2) Event Animated Objects : //-------------------------
        //---------------------------------------------------------
        arrowIcon = {};
        
    
        // (3) Renderers : //--------------------------------------
        //---------------------------------------------------------
        rendererWebGl = {};
        rendererCSS = {};
    
        // (3) Renderers : //--------------------------------------
        //---------------------------------------------------------
        sceneWebGl = {};
        sceneCSS = {};   
    
        // (4) controlsWebGl : //----------------------------------
        //---------------------------------------------------------    
        controlsWebGl = {};
        //controlsCubeWebGl = {};    
    
        // (5) Super objectWebGl : //------------------------------
        //---------------------------------------------------------    
        objectDynamic = {};
    
        //circle = {};

        wall = {};
        
        strs2 = {}; //potrzebny do ustawienia directional light
        
        // (5) Super objectWebGl : //------------------------------
        //---------------------------------------------------------         
        bricsTexture = {};
        
        
        picObjWebGl = [];
        picObjOrder = [];
        
        picCoord = [];
        picCoord['polnocna'] = [];
        picCoord['wschodnia'] = [];
        picCoord['poludniowa'] = [];
        picCoord['zachodnia'] = [];        
        
        picCoord['zachodnia'][1] = new Array(-770,260,915,0,Math.PI/2,0);        
        picCoord['zachodnia'][2] = new Array(-770,260,655,0,Math.PI/2,0);        
        picCoord['zachodnia'][3] = new Array(-770,260,350,0,Math.PI/2,0);         
        picCoord['zachodnia'][4] = new Array(-770,260,110,0,Math.PI/2,0);        
        
        projector = new THREE.Projector();
        mouse_vector = new THREE.Vector3();
        mouse = { x: 0, y: 0, z: 1 };
        ray = new THREE.Raycaster( new THREE.Vector3(0,0,0), new THREE.Vector3(0,0,0) );
        intersects = [];
        
});