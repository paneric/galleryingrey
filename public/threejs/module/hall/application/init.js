/*
===============================================================================
= SpaceWebGl constructor ======================================================
===============================================================================
*/
define('application/init',[

    'application/scene'	    
    ,'application/camera'     
    ,'application/renderer'   
    ,'application/objectStatic'    
    //,'application/objectDynamic'       
    ,'application/light'
    ,'application/controls'     
    ,'application/onMouseClick'
    ,'application/appendRenderer'    
    	
	], function(
    
        scene//function
        ,camera//function
        ,renderer//function       
        ,objectStatic//function
        //,objectDynamic//function
        ,light//function
        ,controls//function         
        ,onMouseClick//code
        ,appendRenderer//function            
    )
    {	
        return function()
        {
            scene();
            camera();
            renderer();
            objectStatic();
            //objectDynamic();
            light();
            controls();            
            appendRenderer();
        }   
    }        
);