/*
===============================================================================
= Camera constructor ==========================================================
===============================================================================
*/
define('application/light', function()
{
    return function()
    {   
    //---------------------------------------------------------------------
    /** Spotlight */
    //--------------------------------------------------------------------- 
        var spotLight = new THREE.SpotLight( 0xffffff,0.25,0);       
        
        spotLight.position.set( 700, 260, 110 );
        if( typeof picObjWebGl[0] !== 'undefined' )
        {
            spotLight.target = picObjWebGl[0].picture;            
        }
        spotLight.angle = Math.PI/3;
        
        spotLight.castShadow = true;

        spotLight.shadow.mapSize.width = 1024;
        spotLight.shadow.mapSize.height = 1024;

        spotLight.shadow.camera.near = 500;
        spotLight.shadow.camera.fear = 4000;
        spotLight.shadow.camera.fow = 30;        

        //sceneWebGl.add( spotLight );

        
        var spotLight = new THREE.SpotLight( 0xffffff,0.15,0);
        spotLight.position.set( 700, 260, 350 );
        if( typeof picObjWebGl[1] !== 'undefined' )
        {
            spotLight.target = picObjWebGl[1].picture;            
        }
        spotLight.angle = Math.PI/3;

        spotLight.castShadow = true;

        spotLight.shadow.mapSize.width = 1024;
        spotLight.shadow.mapSize.height = 1024;

        spotLight.shadow.camera.near = 500;
        spotLight.shadow.camera.far = 4000;
        spotLight.shadow.camera.fov = 30;

        //sceneWebGl.add( spotLight );
        
        
        var spotLight = new THREE.SpotLight( 0xffffff,0.15,0);
        spotLight.position.set( 700, 260, 655 );
        if( typeof picObjWebGl[2] !== 'undefined' )
        {
            spotLight.target = picObjWebGl[2].picture;            
        }
        spotLight.angle = Math.PI/3;

        spotLight.castShadow = true;

        spotLight.shadow.mapSize.width = 1024;
        spotLight.shadow.mapSize.height = 1024;

        spotLight.shadow.camera.near = 500;
        spotLight.shadow.camera.far = 4000;
        spotLight.shadow.camera.fov = 30;

        //sceneWebGl.add( spotLight );        
        

        var spotLight = new THREE.SpotLight( 0xffffff,0.4,0);
        spotLight.position.set( 700, 260, 915 );
        if( typeof picObjWebGl[3] !== 'undefined' )
        {
            spotLight.target = picObjWebGl[3].picture;            
        }
        spotLight.angle = Math.PI/3;

        spotLight.castShadow = true;

        spotLight.shadow.mapSize.width = 1024;
        spotLight.shadow.mapSize.height = 1024;

        spotLight.shadow.camera.near = 500;
        spotLight.shadow.camera.far = 4000;
        spotLight.shadow.camera.fov = 30;

        //sceneWebGl.add( spotLight );        

        //---------------------------------------------------------------------
        /** Ambient light */
        //---------------------------------------------------------------------        
		var lightAmb = new THREE.AmbientLight( 0xffffff, 0.5 );//0xFFF9E0, 0.7
        sceneWebGl.add(lightAmb);    
        
        //---------------------------------------------------------------------
        /** Directional light (wzdluz) */
        //--------------------------------------------------------------------- 
        geometry = new THREE.BoxGeometry( 1, 1, 1 );                
        material = new THREE.MeshPhongMaterial({opacity : 0});                
                
        lightPoint1 = new THREE.Mesh( geometry,material );                
        lightPoint1.position.set(0,0,0);
            
        sceneWebGl.add(lightPoint1);
        
        lightPoint2 = new THREE.Mesh( geometry,material );                
        lightPoint2.position.set(0,0,0);
            
        sceneWebGl.add(lightPoint2);        

        /** Directional light (blizej,lewy) */
        //---------------------------------------------------------------------         
        var lightDir1 = new THREE.DirectionalLight(0xffffff,.2);	
        lightDir1.position.set(-500,500,850);//-600,550,-650      
        lightDir1.target = lightPoint2;
        lightDir1.castShadow = true;//ok
        
        lightDir1.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 50, 1, 1200, 2500 ) );
        lightDir1.shadow.bias = 0.0001;       
        
        lightDir1.shadow.camera.near = 250;//250
        lightDir1.shadow.camera.far = 2000;//4000
        lightDir1.shadow.camera.fov = 1550;//55             
        
        lightDir1.shadow.mapSize.width = 1024;//1024
        lightDir1.shadow.mapSize.height = 1024;//1024
  
		sceneWebGl.add(lightDir1);
		/*
        var sphereSize = 25; 
		var DirectionalLightHelper = new THREE.DirectionalLightHelper( lightDir1, sphereSize );
		sceneWebGl.add( DirectionalLightHelper );
 		*/
        /** Directional light (blizej,prawy) */
        //---------------------------------------------------------------------         
        var lightDir2 = new THREE.DirectionalLight(0xffffff,.2);	
        lightDir2.position.set(500,500,850);//-600,550,-650      
        lightDir2.target = lightPoint2;
        lightDir2.castShadow = true;//ok
        
        lightDir2.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 50, 1, 1200, 2500 ) );
        lightDir2.shadow.bias = 0.0001;       
        
        lightDir2.shadow.camera.near = 250;//250
        lightDir2.shadow.camera.far = 2000;//4000
        lightDir2.shadow.camera.fov = 1550;//55             
        
        lightDir2.shadow.mapSize.width = 1024;//1024
        lightDir2.shadow.mapSize.height = 1024;//1024
  
		sceneWebGl.add(lightDir2);
		/*
        var sphereSize = 25; 
		var DirectionalLightHelper = new THREE.DirectionalLightHelper( lightDir2, sphereSize );
		sceneWebGl.add( DirectionalLightHelper );
		*/
        //---------------------------------------------------------------------
        /** Directional light (dalej,prawy) */
        //--------------------------------------------------------------------- 
        geometry = new THREE.BoxGeometry( 1, 1, 1 );                
        material = new THREE.MeshPhongMaterial({opacity : 0});                
                
        lightPoint3 = new THREE.Mesh( geometry,material );                
        lightPoint3.position.set(0,0,0);
            
        sceneWebGl.add(lightPoint3);
        
        lightPoint4 = new THREE.Mesh( geometry,material );                
        lightPoint4.position.set(0,0,0);//-340,350,500
            
        sceneWebGl.add(lightPoint4);        

        /** Directional light (dalej,prawa) */
        //---------------------------------------------------------------------         
        var lightDir3 = new THREE.DirectionalLight(0xffffff,.2);//0xFFF9E0,0.2	
        lightDir3.position.set(500,500,-850);//-600,550,-650      
        lightDir3.target = lightPoint3;
        lightDir3.castShadow = true;//ok
        
        lightDir3.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 50, 1, 1200, 2500 ) );
        lightDir3.shadow.bias = 0.0001;       
        
        lightDir3.shadow.camera.near = 250;//250
        lightDir3.shadow.camera.far = 250;//4000
        lightDir3.shadow.camera.fov = 1;//55              
        
        lightDir3.shadow.mapSize.width = 1024;//1024
        lightDir3.shadow.mapSize.height = 1024;//1024
  
		sceneWebGl.add(lightDir3);
		/*
        var sphereSize = 25; 
		var DirectionalLightHelper = new THREE.DirectionalLightHelper( lightDir3, sphereSize );
		sceneWebGl.add( DirectionalLightHelper );
 		*/
        /** Directional light (dalej,lewa) */
        //---------------------------------------------------------------------         
        var lightDir4 = new THREE.DirectionalLight(0xffffff,.2);//0xFFF9E0,0.2	
        lightDir4.position.set(-500,500,-850);//-600,550,-650      
        lightDir4.target = lightPoint4;
        lightDir4.castShadow = true;//ok
        
        lightDir4.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 50, 1, 1200, 2500 ) );
        lightDir4.shadow.bias = 0.0001;       
        
        lightDir4.shadow.camera.near = 250;//250
        lightDir4.shadow.camera.far = 250;//4000
        lightDir4.shadow.camera.fov = 1;//55              
        
        lightDir4.shadow.mapSize.width = 1024;//1024
        lightDir4.shadow.mapSize.height = 1024;//1024
  
		sceneWebGl.add(lightDir4);
		/*
        var sphereSize = 25; 
		var DirectionalLightHelper = new THREE.DirectionalLightHelper( lightDir4, sphereSize );
		sceneWebGl.add( DirectionalLightHelper );
		*/		
	//---------------------------------------------------------------------
    /** Point light */
    //---------------------------------------------------------------------         
        var light1f = new THREE.PointLight(0xFFF9E0,0.7,550);
		light1f.position.set(-440,550,850);//500     
		sceneWebGl.add(light1f);

        var sphereSize = 25; 
		var pointLightHelper = new THREE.PointLightHelper( light1f, sphereSize );         
		sceneWebGl.add( pointLightHelper );
         
        var light2f = new THREE.PointLight(0xFFF9E0,0.7,550);
		light2f.position.set(440,550,850);//500
		sceneWebGl.add(light2f);
		/*
		var sphereSize = 25; 
		var pointLightHelper = new THREE.PointLightHelper( light2f, sphereSize ); 
		sceneWebGl.add( pointLightHelper );        
        */

        var light1 = new THREE.PointLight(0xFFF9E0,0.7,650);
		light1.position.set(-440,550,200);//500      
		sceneWebGl.add(light1);

        var sphereSize = 25; 
		var pointLightHelper = new THREE.PointLightHelper( light1, sphereSize );         
		sceneWebGl.add( pointLightHelper );
        
        var light2 = new THREE.PointLight(0xFFF9E0,0.7,650);
		light2.position.set(440,550,200);//500
		sceneWebGl.add(light2);
		/*
		var sphereSize = 25; 
		var pointLightHelper = new THREE.PointLightHelper( light2, sphereSize ); 
		sceneWebGl.add( pointLightHelper );
		*/

        var light1b = new THREE.PointLight(0xFFF9E0,0.7,450);
		light1b.position.set(-440,550,-920);//500            	
		sceneWebGl.add(light1b);

        var sphereSize = 25; 
		var PointLightHelper = new THREE.PointLightHelper( light1b, sphereSize ); 
		sceneWebGl.add( PointLightHelper );
        
        var light2b = new THREE.PointLight(0xFFF9E0,0.7,450);
		light2b.position.set(440,550,-920);//500
		sceneWebGl.add(light2b);
		/*
		var sphereSize = 25; 
		var PointLightHelper = new THREE.PointLightHelper( light2b, sphereSize ); 
		sceneWebGl.add( PointLightHelper );        
        */     
    };        
              
});

/*
Problem ze swiatlem : w przypadku kiedy tworzylem dwa odzielne directional light skierowane na dwa odzielne punkty, wystepowal problem 
z cieniem. W momencie kiedy oba swiatla zostaly utworzone na jednej zmiennej problem zniknal, ZATEM TRZEBA TWORZYC ZRODLA SWIATLA NA JEDNEJ
ZMIENNEJ !!!

*/