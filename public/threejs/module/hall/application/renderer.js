/*
===============================================================================
= RendererWebGl constructor ===================================================
===============================================================================
*/
define('application/renderer', function()
{
    return function()
    {           
        // rendererWebGl Constructor : ------------------------------------------------
        //-----------------------------------------------------------------------------
        if(Detector.webgl){
            rendererWebGl = new THREE.WebGLRenderer({antialias:true});
        } else {
            rendererWebGl = new THREE.CanvasRenderer({antialias:true});
        }       
       
        rendererWebGl.setSize(window.innerWidth, window.innerHeight);    
        rendererWebGl.domElement.style.position = 'absolute';	
        rendererWebGl.domElement.style.top = 0;
        rendererWebGl.domElement.style.left = 0;
        rendererWebGl.domElement.style.zIndex = 0;        
        rendererWebGl.setClearColor(0xffffff, 1);
	
        rendererWebGl.shadowMap.enabled = true;
        rendererWebGl.shadowMap.type = THREE.PCFSoftShadowMap;        
        
        
        
        
        
        
        
        

        //rendererWebGl.shadowMapSoft = true;
	
        //rendererWebGl.setClearColor(scene.fog.color);
        //rendererWebGl.gammaInput = true;
        //rendererWebGl.gammaOutput = true;
        //rendererWebGl.physicallyBasedShading = true;
    
        //document.getElementById('body3d').appendChild( rendererWebGl.domElement );
          
        // rendererCSS constructor : --------------------------------------------------
        //-----------------------------------------------------------------------------        
        rendererCSS = new THREE.CSS3DRenderer();
        rendererCSS.setSize(window.innerWidth, window.innerHeight);
        rendererCSS.domElement.style.position = 'absolute';
        rendererCSS.domElement.style.top = 0;
        rendererCSS.domElement.style.left = 0;
        rendererCSS.domElement.style.zIndex = -1;
    }  
});