/*
===============================================================================
= SceneWebGl constructor ======================================================
===============================================================================
*/
spaceWebGl();

function spaceWebGl()
{
    require.config({
        
        paths: {
            'jQuery': 'jquery-1.10.1.min' // assign proper jquery version for 'jQuery' name
        },
        shim: {
            'jQuery': {
                exports: '$' // if someone use 'jQuery' name, use global '$' variable as module value
            }
        }
    });
    
    require([

        'application/variables'             
        ,'application/init'             
        ,'application/animate'        
          
    ], function(
    
        variables
        ,init
        ,animate
    )
    {
        init();
        animate();              
    });
}