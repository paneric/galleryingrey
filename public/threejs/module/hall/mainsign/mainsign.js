define('module/mainsign/mainsign', function()
{               
        /*
        groundCubeCamera = new THREE.CubeCamera(0.1, 1000, 2048); // parameters: near, far, resolution
        groundCubeCamera.renderTarget.texture.minFilter = THREE.LinearFilter; // mipmap filter
        groundCubeCamera.position.set(0, 0, 0);

        sceneWebGl.add(groundCubeCamera);		
        */
        
    var mainsign = 
    {
        obj : new THREE.Object3D()            
    };
        
        
        
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/mainsign/pane.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial({
                shading: THREE.FlatShading // takie ustawienie jest konieczne dla pozbycia sie smug
                ,color : 0x00213B 
                ,opacity : 1                                         
            });
			var obj = new THREE.Mesh( geometry, material );
			obj.scale.set( 100, 100, 100 );//200
			obj.position.set(-170, -5, 0);//-340,-10,-700
			obj.rotation.set(0,0, 0);
			obj.castShadow = true;
			//sceneWebGl.add( wall );
            mainsign.obj.add( obj );//add a mesh with geometry to it                                
		});
        
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/mainsign/ric.json', function( geometry ){
            var material = new THREE.MeshPhongMaterial({
                shading: THREE.FlatShading // takie ustawienie jest konieczne dla pozbycia sie smug
                ,color : 0x006CFF//0x3C71B9//0x539cff//
                ,opacity : 1
            });
			var obj = new THREE.Mesh( geometry, material );
			obj.scale.set( 100, 100, 100 );
			obj.position.set(0, 0, -10);//0,0,-700
			obj.rotation.set(0,0, 0);
			obj.castShadow = true;
			//sceneWebGl.add( wall );
            mainsign.obj.add( obj );//add a mesh with geometry to it
		});
        
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/mainsign/com.json', function( geometry ){
            var material = new THREE.MeshPhongMaterial({
                shading: THREE.FlatShading // takie ustawienie jest konieczne dla pozbycia sie smug
                ,color : 0x00213B
                ,opacity : 1                
            });
			var obj = new THREE.Mesh( geometry, material );
			obj.scale.set( 100, 100, 100 );
			obj.position.set(160, 0, 0);//320,0,-700
			obj.rotation.set(0,0, 0);
			obj.castShadow = true;
			//sceneWebGl.add( wall );
            mainsign.obj.add( obj );//add a mesh with geometry to it
            
            scaleFactor = window.innerWidth/1680;
            mainsign.obj.scale.set(scaleFactor,scaleFactor,scaleFactor);        
		});       
        
        mainsign.animate = function(){this.obj.rotation.y += 0.005;};
        
        return mainsign;

});