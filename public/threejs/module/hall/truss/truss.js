define('truss/truss', function()
{    		   
    return function()
    {   
        var loader = new THREE.JSONLoader();
		loader.load( base_url + 'threejs/module/hall/truss/truss.json', function( geometry )
        {
			var material = new THREE.MeshPhongMaterial(
            {
                shading: THREE.FlatShading// takie ustawienie jest konieczne dla pozbycia sie smug                              
                ,color: 0xbbbbbb//
                //,envMap: groundCubeCamera.renderTarget
                ,reflectivity: 0.05
                
            });

			truss1 = new THREE.Mesh( geometry, material );
			truss1.scale.set( 100, 100, 100 );
			truss1.position.set(0, 0,0);
			truss1.rotation.set(0, 0, 0);
			truss1.castShadow = true;            
			truss1.receiveShadow = true;  
			
            sceneWebGl.add( truss1 );
            
		});
    }
              
});