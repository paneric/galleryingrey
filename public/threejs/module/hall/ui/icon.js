define('module/ui/icon', function()
{           
    arrowIcon = {obj : new THREE.Object3D()}; 
    
    var loader = new THREE.JSONLoader();
    loader.load(base_url + 'threejs/module/ui/arrows.json', function( geometry )
    {
        var material = new THREE.MeshPhongMaterial
        ({
            shading: THREE.FlatShading// takie ustawienie jest konieczne dla pozbycia sie smug                              
            ,color: 0x003F73
        });

        var obj = new THREE.Mesh( geometry, material );
        obj.scale.set( 2.5, 2.5, 2.5 );
        obj.position.set(0, 0, 0);
        obj.rotation.set(0, 0, 0);
        obj.castShadow = true;
    
        arrowIcon.obj.add( obj );//add a mesh with geometry to it 
    });
             

    
    var material = new THREE.MeshPhongMaterial    
    ({
        side : THREE.DoubleSide
        ,overdraw : 0.99 //w celu pozbycia sie diagonalnych w przypadku wyswietlania przy uzyciu canvas
        ,color : 0xe3e3e3
        ,opacity : 0.2	   
    });    
       
    var geometry = new THREE.PlaneBufferGeometry(50,39);
    var obj = new THREE.Mesh(geometry, material);
        
    arrowIcon.obj.add( obj );//add a mesh with geometry to it 
    arrowIcon.obj.position.set(0,63,0);





        
        /*
        loader = new THREE.JSONLoader();
		loader.load( 'threejs/module/ui/ring.json', function( geometry ){
			material = new THREE.MeshPhongMaterial(
            {
                color: 0x003F73
                ,shading: THREE.FlatShading
                ,opacity : .6
            });// takie ustawienie jest konieczne dla pozbycia sie smug

			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 13, 20, 13 );
			wall.position.set(0, -200, 0);
			wall.rotation.set(0,0, 0);
			wall.castShadow = true;

			sceneWebGl.add( wall );
		});
        
        */
        
        /*
        var loader = new THREE.ColladaLoader();
        loader.options.convertUpAxis = true;

        loader.load( 'threejs/module/ui/gold.dae', function ( collada ) 
        {
            gold = collada.scene;
            var skin = collada.skins[ 0 ];
            gold.position.set(0,-120,0);
            gold.scale.set(60,60,60);
            sceneWebGl.add( gold );	
        });        
       */ 
        
        
        /*
        var loader = new THREE.ColladaLoader();
        loader.options.convertUpAxis = true;

        loader.load( 'threejs/module/ui/arrows.dae', function ( collada ) 
        {
            arrowIcon = collada.scene;
            var skin = collada.skins[ 0 ];
            arrowIcon.position.set(0,62,0);
            arrowIcon.scale.set(2.5,2.5,2.5);
            sceneWebGl.add(arrowIcon);
            //console.log(arrowIcon);	
        });
        */

});