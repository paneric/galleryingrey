/*
===============================================================================
= Ground constructor ==========================================================
===============================================================================
*/
define('ceiling/ceiling', function()
{    
    return function()
    {
        var loader = new THREE.JSONLoader();
		loader.load( base_url + 'threejs/module/hall/ceiling/r2c1_1.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 0.99
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3              
            });
			ceilC1 = new THREE.Mesh( geometry, material );
			ceilC1.scale.set( 100, 100, 100 );
			ceilC1.position.set(0, 0, 0);
			ceilC1.rotation.set(0,0, 0);
			ceilC1.castShadow = true;
            
			sceneWebGl.add( ceilC1 );
		}); 
        
        var loader = new THREE.JSONLoader();
		loader.load( base_url + 'threejs/module/hall/ceiling/r2c1_2.json', function( geometry ){
	       
           bricsTexture.wrapS = bricsTexture.wrapT = THREE.RepeatWrapping; 
	       bricsTexture.repeat.set( 8, 8 ); //5.7x           
            
            
            var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,map: bricsTexture
                //,side : THREE.DoubleSide
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.99
                //,shininess: 10                
            });
			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.castShadow = true;
            
			sceneWebGl.add( wall );
		});        
        
        var loader = new THREE.JSONLoader();
		loader.load( base_url + 'threejs/module/hall/ceiling/r2c1_3.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 0.99
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3              
            });
			ceilC1 = new THREE.Mesh( geometry, material );
			ceilC1.scale.set( 100, 100, 100 );
			ceilC1.position.set(0, 0, 0);
			ceilC1.rotation.set(0,0, 0);
			ceilC1.castShadow = true;
            
			sceneWebGl.add( ceilC1 );
		});        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        /*
        
        var loader = new THREE.JSONLoader();
		loader.load( 'threejs/module/ceiling/C1F.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0x003f73 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3              
            });
			ceilC1F = new THREE.Mesh( geometry, material );
			ceilC1F.scale.set( 100, 100, 100 );
			ceilC1F.position.set(-1960, 490, 0);
			ceilC1F.rotation.set(0,0, 0);
			ceilC1F.castShadow = true;
            
			//sceneWebGl.add( ceilC1F );
		});        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

        var loader = new THREE.JSONLoader();
		loader.load( 'threejs/module/ceiling/C31.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3                
            });
			ceilC31 = new THREE.Mesh( geometry, material );
			ceilC31.scale.set( 100, 100, 100 );
			ceilC31.position.set(980, 360, 0);
			ceilC31.rotation.set(0,0, 0);
			ceilC31.castShadow = true;
            
			//sceneWebGl.add( ceilC31 );
		});
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        var loader = new THREE.JSONLoader();
		loader.load( 'threejs/module/ceiling/C2C3.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,reflectivity: 0.3               
            });
			ceilC2C3 = new THREE.Mesh( geometry, material );
			ceilC2C3.scale.set( 100, 100, 100 );
			ceilC2C3.position.set(380, 760, 0);
			ceilC2C3.rotation.set(0,0, 0);
			ceilC2C3.castShadow = true;
            
			//sceneWebGl.add( ceilC2C3 );
		});
        
        var loader = new THREE.JSONLoader();
		loader.load( 'threejs/module/ceiling/C2C3F.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0x003f73 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3              
            });
			ceilC2C3F = new THREE.Mesh( geometry, material );
			ceilC2C3F.scale.set( 100, 100, 100 );
			ceilC2C3F.position.set(980, 730, 0);
			ceilC2C3F.rotation.set(0,0, 0);
			ceilC2C3F.castShadow = true;
            
			//sceneWebGl.add( ceilC2C3F );
		});        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
                
        
        
        var loader = new THREE.JSONLoader();
		loader.load( 'threejs/module/ceiling/C4.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3               
            });
			ceilC4 = new THREE.Mesh( geometry, material );
			ceilC4.scale.set( 100, 100, 100 );
			ceilC4.position.set(2720, 460, 0);
			ceilC4.rotation.set(0,0, 0);
			ceilC4.castShadow = true;
            
			//sceneWebGl.add( ceilC4 );
		});
        
        var loader = new THREE.JSONLoader();
		loader.load( 'threejs/module/ceiling/C1F.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                ,color: 0x003f73 
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3              
            });
			ceilC4F = new THREE.Mesh( geometry, material );
			ceilC4F.scale.set( 100, 100, 100 );
			ceilC4F.position.set(2720, 490, 0);
			ceilC4F.rotation.set(0,0, 0);
			ceilC4F.castShadow = true;
            
			//sceneWebGl.add( ceilC4F );
		});      
        */
               
    }
});