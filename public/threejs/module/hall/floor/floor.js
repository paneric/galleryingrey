/*
===============================================================================
= Ground constructor ==========================================================
===============================================================================
*/
define('floor/floor', function()
{    		   
    return function()
    {
        var textureLoader = new THREE.TextureLoader();
        var r2f1Texture = textureLoader.load(base_url + 'threejs/module/hall/floor/floor3.jpg' );
        r2f1Texture.wrapS = r2f1Texture.wrapT = THREE.RepeatWrapping; 
        r2f1Texture.repeat.set( 4, 5.5 ); //5.7x

        var material = new THREE.MeshPhongMaterial( 
        {
                opacity : 1
                ,map: r2f1Texture
                //,side : THREE.DoubleSide
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,envMap: r2f1CubeCamera.renderTarget
                ,reflectivity: 0.3
                ,shininess: 10
        });        

        var width = 1600;
        var length = 2200;
        
        var geometry = new THREE.PlaneBufferGeometry( width, length);

        var plane = new THREE.Mesh( geometry, material );
        
        plane.receiveShadow = true;
        
        plane.position.set(0, 0, 0);
        plane.rotation.x = -Math.PI/2;        
        
        sceneWebGl.add( plane );




        
        
        
        var material = new THREE.MeshPhongMaterial(
        {
                opacity : .05
                ,transparent : true
                //,map: r2f1Texture
                //,side : THREE.DoubleSide
                ,color: 0xffffff
                ,shading: THREE.FlatShading
                ,envMap: r2f1CubeCamera.renderTarget.texture
                ,reflectivity: 1
                //,shininess: 10
        });

        var radius = 2200;
        var segments = 256;

        var circleGeometry = new THREE.CircleGeometry(radius, segments);				

        circle = new THREE.Mesh( circleGeometry, material );        
        circle.position.set(0, 10, 0);
        circle.rotation.x = -Math.PI/2;

        sceneWebGl.add( circle );
        
        
        
        
        
        
        /*
        var r2f1Texture = new THREE.ImageUtils.loadTexture( 'threejs/module/floor/floor3.jpg' ); 
        
        var loader = new THREE.JSONLoader();
		loader.load( 'threejs/module/floor/r2f1.json', function( geometry ){
	       
           r2f1Texture.wrapS = r2f1Texture.wrapT = THREE.RepeatWrapping; 
	       r2f1Texture.repeat.set( 4, 5.5 ); //5.7x           

            var material = new THREE.MeshLambertMaterial(
            {
                opacity : 1.0
                //,map: r2f1Texture
                //,side : THREE.DoubleSide
                ,color: 0xffffff 
                ,shading: THREE.FlatShading
                //,envMap: r2f1CubeCamera.renderTarget
                ,reflectivity: 0.3
                //,shininess: 10            
            });
			r2f1 = new THREE.Mesh( geometry, material );
			r2f1.scale.set( 100, 100, 100 );
			r2f1.position.set(0, 0, 0);
			r2f1.rotation.set(0,0, 0);
			r2f1.castShadow = true;
            
			sceneWebGl.add( r2f1 );
		});
        
        */
    }
              
});