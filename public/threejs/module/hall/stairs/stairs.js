define('stairs/stairs', function()
{    		   
    return function()
    {   
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/stairs/stairs.json', function( geometry )
        {
			var material = new THREE.MeshPhongMaterial(
            {
                shading: THREE.FlatShading// takie ustawienie jest konieczne dla pozbycia sie smug                            
                //,envMap: groundCubeCamera.renderTarget
                ,reflectivity: 0.05
                
            });

			strs2 = new THREE.Mesh( geometry, material );
			strs2.scale.set( 100, 100, 100 );
			strs2.position.set(0, 0,0);
			strs2.rotation.set(0, 0, 0);
			strs2.castShadow = true;            
//console.log(strs2);
            sceneWebGl.add( strs2 );
		});
        
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/hall/stairs/steps.json', function( geometry )
        {   
            var material = new THREE.MeshPhongMaterial(
            {
                opacity : 1.0
                //,map: r2stepsTexture
                //,side : THREE.DoubleSide,
                ,color: 0x000000
                ,shading: THREE.FlatShading
                ,reflectivity: 0.99
                ,shininess: 99
            });

			steps1 = new THREE.Mesh( geometry, material );
			steps1.scale.set( 100, 100, 100 );
			steps1.position.set(0, 0,0);
			steps1.rotation.set(0, 0, 0);
			steps1.castShadow = true;            

            sceneWebGl.add( steps1 );
		});

    //---------------------------------------------------------------------
    /** reling */
    //---------------------------------------------------------------------

        var loader = new THREE.ColladaLoader();
        
        loader.load(base_url + 'threejs/module/hall/stairs/reling.dae', function colladaReady( collada ) 
        {   
			//collada.scene.castShadow = true;            
            
            //console.log(collada.scene.children);
            
            var reling1 = collada.scene.children[0].children[0];
            reling1.scale.set( 100, 100, 100 );
			reling1.position.set(0, 0, 0);
			reling1.rotation.set(-Math.PI/2, 0, 0);          
            reling1.material.color.set(0xffffff);
            reling1.material.opacity = 0.5;
            reling1.material.transparent = true;            
            
            var reling2 = collada.scene.children[1].children[0];
            reling2.scale.set( 100, 100, 100 );
			reling2.position.set(0, 0, 0);
			reling2.rotation.set(-Math.PI/2, 0, 0);          
            reling2.material.color.set(0xffffff);
            //reling2.material.opacity = 0.5;

            var reling3 = collada.scene.children[2].children[0];
            reling3.scale.set( 100, 100, 100 );
			reling3.position.set(0, 0, 0);
			reling3.rotation.set(-Math.PI/2, 0, 0);          
            reling3.material.color.set(0xffffff);
            //reling3.material.opacity = 0.5;
            
            sceneWebGl.add( reling1 ); 
            sceneWebGl.add( reling2 );
            sceneWebGl.add( reling3 );                    
        });


        
        /*
        var loader = new THREE.ColladaLoader();
        loader.options.convertUpAxis = true;

        loader.load( 'threejs/module/stairs/steps.dae', function colladaReady( collada ) 
        {   
            wall = collada.scene.children[0].children[0];
            wall.material.map.repeat.set(10,2)
            wall.scale.set( 100, 100, 100 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.castShadow = true;
            
			sceneWebGl.add( wall ); 

        });
        */
        
        
        
    }
              
});

/*
"mat_glass": {
"type": "MeshPhongMaterial",
"parameters": 
{ "color": 460810, "envMap": "my_cubemap", "reflectivity": 0.02, "specular":1973791,"shininess":600, "opacity":0.6,"combine":"MixOperation","blending":"NormalBlending"
},
*/