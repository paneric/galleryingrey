/*
===============================================================================
= Ground constructor ==========================================================
===============================================================================
*/
define('paint/paint', function()
{    
    return function()
    {		        
        if( typeof gall_repo !== 'undefined' )
        {
            iObj = -1;
            for (var iSide in picCoord)
            {    
                for(var iPic in picCoord[iSide])
                {                    
                    if( typeof gall_repo[iSide][iPic] !== 'undefined' )
                    {        
                        iObj++;                
                
                        picObjWebGl[iObj] = {};
                        picObjWebGl[iObj].id = gall_repo[iSide][iPic]['id'];
                                        
                        var width  = gall_repo[iSide][iPic]['szerokosc']*2; // 2 jest tutaj przyjeta stala
                        var length = gall_repo[iSide][iPic]['dlugosc']*2; // 2 jest tutaj przyjeta stala
                        console.log(width+'  '+length+'  '+gall_repo[iSide][iPic]['name']);
 
                        picObjWebGl[iObj].geometry = new THREE.BoxGeometry( width, length, 3 );                
                
                        picObjWebGl[iObj].texture = new THREE.TextureLoader(site_url + '/home/home/serve_image/' + picObjWebGl[iObj].id);            
                        picObjWebGl[iObj].texture.wrapS = picObjWebGl[iObj].texture.wrapT = THREE.RepeatWrapping; 
                        picObjWebGl[iObj].texture.repeat.set( 1, 1 );
            
                        picObjWebGl[iObj].material = new THREE.MeshPhongMaterial(
                        {
                            opacity : 1.0
                            ,map: picObjWebGl[iObj].texture
                            //,side : THREE.DoubleSide
                            ,color: 0xffffff 
                            ,shading: THREE.FlatShading
                            //,envMap: r2f1CubeCamera.renderTarget
                            ,reflectivity: 0.3
                            ,shininess: 10
                        });                
                
                        picObjWebGl[iObj].picture = new THREE.Mesh( picObjWebGl[iObj].geometry, picObjWebGl[iObj].material );        
                        picObjWebGl[iObj].picture.castShadow = true;        
                        picObjWebGl[iObj].picture.rotation.y = Math.PI/2;         
                        picObjWebGl[iObj].picture.position.set(picCoord[iSide][iPic][0], picCoord[iSide][iPic][1], picCoord[iSide][iPic][2]);
            
                        sceneWebGl.add( picObjWebGl[iObj].picture );                    
                    }           
                } 
            }
        }
    }
     
});