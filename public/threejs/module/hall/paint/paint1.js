/*
===============================================================================
= Ground constructor ==========================================================
===============================================================================
*/
define('paint/paint', function()
{    
    return function()
    {		
        if( typeof gall_repo !== 'undefined' )
        {
            for (var iSide in picCoord)
            {    
                for(var iObj in picCoord[iSide])
                {
                    iPic = gall_repo[iSide][iObj]['id'];                
                
                    picObjWebGl[iPic] = {};
                
                    var width  = gall_repo[iSide][iObj]['szerokosc']*2; // 2 jest tutaj przyjeta stala
                    var length = gall_repo[iSide][iObj]['dlugosc']*2; // 2 jest tutaj przyjeta stala
                    
                    console.log(width+'  '+length+'  '+gall_repo[iSide][iObj]['path']);

                    picObjWebGl[iPic].geometry = new THREE.BoxGeometry( width, length, 3 )                
            
                    picObjWebGl[iPic].material = new THREE.MeshPhongMaterial( 
                    {
                        opacity : 1.0
                        //,map: picObjWebGl[iPic].texture
                        //,side : THREE.DoubleSide
                        ,color: 0xffffff 
                        ,shading: THREE.FlatShading
                        //,envMap: r2f1CubeCamera.renderTarget
                        ,reflectivity: 0.3
                        ,shininess: 10
                        ,blending: THREE.NoBlending // niezbedne dla widocznosci cssElement
                    });                
                
                    picObjWebGl[iPic].picture = new THREE.Mesh( picObjWebGl[iPic].geometry, picObjWebGl[iPic].material );        
                    picObjWebGl[iPic].picture.castShadow = true;        
                    picObjWebGl[iPic].picture.rotation.y = Math.PI/2;         
                    picObjWebGl[iPic].picture.position.set(picCoord[iSide][iObj][0], picCoord[iSide][iObj][1], picCoord[iSide][iObj][2]);
            
                    sceneWebGl.add( picObjWebGl[iPic].picture );

                    
                    picObjWebGl[iPic].cssElement = document.createElement( 'div' );
                    picObjWebGl[iPic].cssElement.id = 'paint'+iPic;
                    picObjWebGl[iPic].style.width = gall_repo[iSide][iObj]['szerokosc']*2;
                    picObjWebGl[iPic].style.height = gall_repo[iSide][iObj]['dlugosc']*2;                                     
                    
                    picObjWebGl[iPic].style.style.backgroundImage = "url('" + + "')";
                    
                    //echo base_url('index.php/rre/serve_image?image='.$image);
                    
                    
                    
                    
                    //<div id='logo' style='background:url(picture.jpg)'></div>
                    
                    
                    
                    
                    
                    picObjWebGl[iPic].cssElement.src = 'textures/sprites/ball.png';
                    // create the object3d for this element
                    picObjWebGl[iPic].cssObj = new THREE.CSS3DObject( picObjWebGl[iPic].cssElement );
                    // we reference the same position and rotation 
                    picObjWebGl[iPic].cssObj.position = picObjWebGl[iPic].picture.position;
                    picObjWebGl[iPic].cssObj.rotation = picObjWebGl[iPic].picture.rotation;
                    // add it to the css scene
                    cssScene.add(picObjWebGl[iPic].cssObj);
                    
                    
                    
                    
                    
                                    
                } 
            }
        }
    }
     
});