
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
    
	<link rel="stylesheet" type="text/css" href="css/home.css" />
</head>
<body id="body00">

	<script type="text/javascript" src="js_webgl/Detector.js"></script>
	<script type="text/javascript" src="js_webgl/three.js"></script>
	<script type="text/javascript" src="js_webgl/Projector.js"></script>
	<script type="text/javascript" src="js_webgl/OrbitControls.js"></script>
	<script type="text/javascript" src="js_webgl/ColladaLoader.js"></script>
	<script type="text/javascript" src="js_webgl/CSS3DRenderer.js"></script>
	<script type="text/javascript" src="js_webgl/FresnelShader.js"></script>	
	
	<script data-main="./threejs/module/hall/hall"       src="js_core/requirejs.js"></script>

</body>
</html>
<script>var base_url = '';</script>
<script>var site_url = 'http://galleryingrey/index.php';</script>
